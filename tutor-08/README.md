### Tutor 08

> Optional Feature

* Design: Apply Multi Column Responsive List, for Tag, and Archive

* Post: Header, Footer, Navigation

* Search: lunr.js

> Configuration

* Filter: keyJoin

* Some Shortcodes

> Blog Content

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Mathjax

* Post: Syntax Highlight: Bulma CSS Reset and Add Prism CSS

* Stylesheet: Bulma Title: CSS Fix

> Finishing

* Layout: Service

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

* Metadata: Complete Author Info

![Eleventy Bulma MD: Tutor 07][11ty-bulma-md-preview-07]

-- -- --

What do you think ?

[11ty-bulma-md-preview-07]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-07/11ty-bulma-md-preview.png
