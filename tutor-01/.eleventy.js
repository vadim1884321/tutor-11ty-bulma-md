module.exports = function(eleventyConfig) {

  // Return your Config object
  return {
    // Templating Engine
    templateFormats: [
      "njk",
      "html"
    ],

    // Directory Management
    dir: {
      // default
      input: "views",
      output: "_site",
      // ⚠️ This value is relative to your input directory.
      includes: "_includes",
    }
  };
};
