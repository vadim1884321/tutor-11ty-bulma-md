module.exports = function(eleventyConfig) {

  // Return your Config object
  return {
    // URL Related
    pathPrefix: "/",

    // Templating Engine
    templateFormats: [
      "md",
      "njk",
      "html"
    ],

    markdownTemplateEngine: false,
    htmlTemplateEngine: "njk",
    dataTemplateEngine: false,

    // Directory Management
    dir: {
      // default
      input: "views",
      output: "_site",
      // ⚠️ This value is relative to your input directory.
      includes: "_includes",
    }
  };
};
