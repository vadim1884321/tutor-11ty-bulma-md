### Tutor 02

> Generate Only Pure HTML

* Configuration: Basic

* Setup Directory for Minimal Eleventy

* Basic Layout: Base, Page, Post

* Partials: Site Wide: HTML Head, Header, Footer

* Basic Content: Page and Post

* Nunjucks: Basic Loop

![Eleventy Bulma MD: Tutor 01][11ty-bulma-md-preview-01]

-- -- --

What do you think ?

[11ty-bulma-md-preview-01]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-01/11ty-bulma-md-preview.png
