---
layout    : post
title     : Marilyn Manson - Sweet Dreams
date      : 2015-07-15 07:35:05
tags      : ["industrial metal", "90s"]
---

Sweet dreams are made of this\
Who am I to disagree?

Travel the world and the seven seas\
Everybody's looking for something
<!--more-->

Some of them want to use you\
Some of them want to get used by you\
Some of them want to abuse you\
Some of them want to be abused
