const moment = require("moment");

module.exports = function(eleventyConfig) {

  // Directory Management
  eleventyConfig.addPassthroughCopy("assets");

  // Layout Alias
  eleventyConfig.addLayoutAlias("home",     "layouts/home.njk");
  eleventyConfig.addLayoutAlias("page",     "layouts/page.njk");
  eleventyConfig.addLayoutAlias("post",     "layouts/post.njk");
  eleventyConfig.addLayoutAlias("archive",  "layouts/archive.njk");
  eleventyConfig.addLayoutAlias("tags",     "layouts/tags.njk");
  eleventyConfig.addLayoutAlias("tag-name", "layouts/tag-name.njk");
  
  // Miscellanous Filters
  
  // Copy paste from Jérôme Coupé
  eleventyConfig.addNunjucksFilter("date", function(date, format) {
    return moment(date).format(format);
  });
  
  // values Filter: MDN web docs
  eleventyConfig.addNunjucksFilter("keys", function(array) {
    return Object.keys(array);
  });

  // Miscellanous Collection

  // Copy paste from Zach
  eleventyConfig.addCollection("tagList",
    require("./views/_11ty/getTagList"));

  // Return your Config object
  return {
    // URL Related
    pathPrefix: "/",

    // Templating Engine
    templateFormats: [
      "md",
      "njk",
      "html"
    ],

    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: false,

    // Directory Management
    passthroughFileCopy: true,    
    dir: {
      input: "views",
      output: "dist",
      // ⚠️ These values are both relative to your input directory.
      includes: "_includes",
      data: "_data"
    }
  };
};
